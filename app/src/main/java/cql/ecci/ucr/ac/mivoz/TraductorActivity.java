package cql.ecci.ucr.ac.mivoz;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.mlkit.common.model.DownloadConditions;
import com.google.mlkit.nl.translate.TranslateLanguage;
import com.google.mlkit.nl.translate.Translation;
import com.google.mlkit.nl.translate.Translator;
import com.google.mlkit.nl.translate.TranslatorOptions;

public class TraductorActivity extends AppCompatActivity
{
    // Create an English-German translator:
    private TranslatorOptions inglesEspannol =
            new TranslatorOptions.Builder()
                    .setSourceLanguage(TranslateLanguage.ENGLISH)
                    .setTargetLanguage(TranslateLanguage.SPANISH)
                    .build();

    private TranslatorOptions espannolIngles =
            new TranslatorOptions.Builder()
                    .setSourceLanguage(TranslateLanguage.SPANISH)
                    .setTargetLanguage(TranslateLanguage.ENGLISH)
                    .build();

    private final Translator traductorEsEn =
            Translation.getClient(espannolIngles);

    private final Translator traductorEnEs =
            Translation.getClient(inglesEspannol);

    private boolean traduccionLista = false;

    private EditText inputTexto;
    private RadioGroup radioGroup;
    private TextView textoTraducido;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_traductor);

        descargarLenguajes();

        inputTexto = findViewById(R.id.editTextTraductor);
        radioGroup = findViewById(R.id.radioGroup);
        textoTraducido = findViewById(R.id.textoTraducido);

        final Button traducir = findViewById(R.id.buttonTraducir);
        traducir.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                switch (radioGroup.getCheckedRadioButtonId())
                {
                    case R.id.radioButtonEnEs:
                        traducir(inputTexto.getText().toString(), traductorEnEs);
                        break;
                    case R.id.radioButtonEsEn:
                        traducir(inputTexto.getText().toString(), traductorEsEn);
                        break;
                }
            }
        });

    }

    public void traducir(String texto, Translator traductor)
    {
        // Se necesita que los lenguajes esten descargados para hacer la traduccion.
        if(traduccionLista == true)
        {
            traductor.translate(texto)
                    .addOnSuccessListener(
                            new OnSuccessListener<String>() {
                                @Override
                                public void onSuccess(@NonNull String translatedText)
                                {
                                    textoTraducido.setText(translatedText);
                                }
                            })
                    .addOnFailureListener(
                            new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e)
                                {
                                    textoTraducido.setText("Oh por Dios, hay un error");
                                }
                            });
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        traductorEnEs.close();
    }



    public void descargarLenguajes()
    {
        DownloadConditions conditions = new DownloadConditions.Builder()
            .requireWifi()
            .build();

        traductorEnEs.downloadModelIfNeeded(conditions)
            .addOnSuccessListener(
                new OnSuccessListener<Void>()
                {
                    @Override
                    public void onSuccess(Void v)
                    {
                        // Model downloaded successfully. Okay to start translating.
                        // (Set a flag, unhide the translation UI, etc.)
                        traduccionLista = true;
                    }
                })
            .addOnFailureListener(
                new OnFailureListener()
                {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        // Model couldn’t be downloaded or other internal error.
                        // ...
                    }
                });
    }
}